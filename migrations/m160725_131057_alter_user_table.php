<?php

use yii\db\Migration;

class m160725_131057_alter_user_table extends Migration
{
    public function up()
    {
		$this->addColumn('user','categoryId','Integer');
    }

    public function down()
    {
       $this->dropColumn('user','categoryId');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
