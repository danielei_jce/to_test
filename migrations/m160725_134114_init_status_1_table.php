<?php

use yii\db\Migration;

class m160725_134114_init_status_1_table extends Migration
{
    public function up()
    {
		$this->createTable(
            'status_1',
            [
                'id' => 'pk',
                'name' => 'string'			
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
        $this->dropTable('status_1');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
